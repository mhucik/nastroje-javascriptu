# Nástroje Javascriptu

 - repozitář se vzorovými soubory: https://github.com/fczbkk/workshop-js-tools
 
 ## 1.NPM
  - tag "name" - identifikuje projekt na npm
    - je dobré používat k němu i namespace, takže např. @mhucik/muj-projekt
  - `dependecies` nebo `dev-dependecies` umí načítat i soubory mimo npm (z gitu, konrétního souboru na disku)   
  - `npm install -D lodash` 
    - instaluju do devDependencies
  - dřív byly ještě peerDependencies, které obsahovaly závislosti, které nenainstaloval `npm install`, protože předpokládal, že danou závislost máme už u sebe
  - node-check-updates - nějaká knihovna, která zkontroluje možnosti updatu (https://www.npmjs.com/package/npm-check-updates) 
  - příkaz `npm ci` podívá se do package.json a jen stáhne závislosti, je rychlejší, nekontroluje vulnerabilities apod
  ### Globální závislosti
  - instalace do globálního namespace
  - seznam `npm ls -g depth=0`
  - `npm install -g balicek` - instalace globálního balíčku
  ### Složka node_modules
    - patří do .gitignore
    - do node_modules se instalují i knihovny pro konkrétní platformu (typicky obrázky)
    - dřív býval hodně zanořená struktura, čímž se instalace, update a celkově práce s tímto komplikovala
 ## 2.YARN
   - pomalý vývoj npm způsobil vznik yarnu
   - Yarn 2 - přímá podpora pro monorepa
   - momentálně stejný jako npm v podstatě
   - gossip: npm má nějaký problém s vývojáři, takže je možné, že se zase zaseká
 ## 3.Nástroje pro buildování (babel, webpack)
 - pokud mám zbuildovaný projekt a při vývoji odmažu nějaký soubor, tak to v tom buildu pořád zůstává!
   - můžu ten build před novým buildem smazat
   - například rimraf (https://www.npmjs.com/package/rimraf)
 ### Scriptování
 - pokud chci concatovat dva příkazy dohromady, ale nechci to složitě psát, můžu použít `pre` nebo `post` společně s požadovaným příkazem. Například mám nadefinované scripty `prebuild`, `postbuild` a `build`. Při spuštění příkazu `npm run build` se nejprve spustí `prebuild`, pak `build` a následně `postbuild`. Můžu udělat i `preprebuild`
 - `||` - spustí se první příkaz před `||` a pokud skončí chybou, spustí se ten druhý
 - `--` - pomocí dvojteček můžu modifikovat přepínače uvnitř spuštěného scriptu. Například při scriptu `"build-alternative": "npm run build -- --out-dir ./build-alternative"` se při spuštění `npm run build-alternative` parametr `--out-dir` přidá do `npm run build` a ne do spuštěného `npm run build-alternative`
 ### Webpack
 ## 4.Testování
 - assert je relativně jednodušší na použití a pochopení, ale hodně nepřehledný
 - chai "assert" na steroidech
 - je dobré dávat přípony testům, kvůli watcherům, includům apod. navíc pak můžu mít testy vedle testovaného kódu
