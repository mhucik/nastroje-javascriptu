module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",//použije pravidla z https://eslint.org/docs/rules/ které mají checkmark
    "parserOptions": {
        "ecmaVersion": 2016,
        "sourceType": "module"
    },
    "rules": {//doplním si svoje vlastní pravidla
        "no-unused-vars": [//aby to hned neřvalo, když píšu kód
            "warn"
        ],
        "no-console": [//kvůli debugu, aby to zase neřvalo error
            "warn"
        ],
        "indent": [
            "error",
            2
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
