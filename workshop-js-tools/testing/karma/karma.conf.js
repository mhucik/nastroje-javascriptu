module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai'],
    files: [
      './*.spec.js'
    ],
    preprocessors: {
      './*.spec.js': ['webpack']
    },
    webpack: {
      mode: 'development',
      devtool: 'source-map',
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /\.spec\.js$/,
          }
        ]
      }
    },
    reporters: ['mocha'],//, 'progress', 'nyan'
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,//mělo by mít opačnou hodnotu od singleRun, pokud chci spustit script jen jednou a ukončit ho, nemá smysl mít spuštěný autowatch
    browsers: [
      'PhantomJS',
      'ChromeHeadless'
    ],
    singleRun: true
  })
}
