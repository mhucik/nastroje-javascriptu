import sinon from 'sinon'
import { assert } from 'chai'
import { callLater } from './index'

describe('Time', function () {

  let clock;

  before(function () {
    clock = sinon.useFakeTimers()
  })

  after(function () {
    clock.restore()
  })

  it('should wait a second', function () {
    const mockFunction = sinon.fake()
    callLater(mockFunction, 1)
    assert(!mockFunction.called)
    clock.tick(999)//proběhne okamžitě, posune ihned o 999 milisekund dopředu
    assert(!mockFunction.called)//ještě není dovršen ten delay, takže by to ještě nemělo být zavolané
    clock.tick(1)//posunu o milisekund, už jsem tedy na 1 sekundě
    assert(mockFunction.called)//a nyní by funkce již měla být zavolaná
  })

  it('should use specific time', function () {
    const myBirthday = new Date(1978, 3, 18)
    clock = sinon.useFakeTimers(myBirthday.getTime())
    const thisYear = (new Date()).getYear()
    assert.equal(thisYear, 78)
  })

})
